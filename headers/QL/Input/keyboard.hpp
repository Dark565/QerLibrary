#pragma once

namespace ql {
    namespace keyboard {
        enum class Key : char {

        };

        enum class Press : char {
            up,
            down
        };
    }
}