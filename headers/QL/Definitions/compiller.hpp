#pragma once


#define QL_ATR_PACKED       __attribute__((packed))
#define QL_ATR_CONSTRUCTOR  __attribute__((constructor))
#define QL_ATR_INLINE       __attribute__((always_inline))