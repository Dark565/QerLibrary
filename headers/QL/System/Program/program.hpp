#pragma once

namespace ql {

    /*Gets argc of the program*/
    int getArgc();

    /*Gets argv of the program*/
    char** getArgv();

}