#pragma once

#include "Colorize/colors.hpp"
#include "Gui/clipboard.hpp"
#include "Pixmap/pixmap.hpp"
#include "Video/video.hpp"
#include "Windows/window.hpp"
#include "WinServer/man.hpp"
#include "Display/display.hpp"