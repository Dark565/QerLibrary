#include <QL/System/Info/version.hpp>

const char* ql::getName() {
    return QL_NAME;
}

const char* ql::getVersion() {
    return QL_VERSION;
}

const char* ql::getRelease() {
    return QL_RELEASE;
}
